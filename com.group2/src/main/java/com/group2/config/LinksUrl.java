package com.group2.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class LinksUrl {

	@Value("${spring.url.back}")
	private String linksUrlSpringboot;
	
	@Value("${spring.url.front}")
	private String linksUrlSpringMvc;

	public String getLinksUrlSpringboot() {
		return linksUrlSpringboot;
	}

	public String getLinksUrlSpringMvc() {
		return linksUrlSpringMvc;
	}


}
