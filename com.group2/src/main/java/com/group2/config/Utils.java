package com.group2.config;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Utils {

	public static List<String> codeMail = new ArrayList<String>();
	
	public static String getStringSearch(String text) {
		String searchText = "%"+text+"%";
		return searchText;
	}
	
	public static String generateMailCode(String userName) {
		Random rand = new Random();
        Integer ranNum = rand.nextInt(9999999)+1000001;
        String code =userName+ranNum.toString();
        codeMail.add(code);
		return code;
	}
}
