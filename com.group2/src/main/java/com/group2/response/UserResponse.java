package com.group2.response;

import com.fasterxml.jackson.annotation.JsonProperty;

public class UserResponse {
	@JsonProperty("firstName")
	private String firstName;
	
	@JsonProperty("lastName")
	private String lastName;
	
	@JsonProperty("username")
	private String userName;
	
	@JsonProperty("password")
	private String password;
	
	@JsonProperty("email")
	private String email;
	
	@JsonProperty("previous_scores")
	private Double previousScores;
	
	@JsonProperty("current_scores")
	private Double currentScores;

	@JsonProperty("role_name")
	private String roleName;
	
	@JsonProperty("status")
	private int status;
	
	public Double getPreviousScores() {
		return previousScores;
	}
	public void setPreviousScores(Double previousScores) {
		this.previousScores = previousScores;
	}
	public Double getCurrentScores() {
		return currentScores;
	}
	public void setCurrentScores(Double currentScores) {
		this.currentScores = currentScores;
	}
	public String getRoleName() {
		return roleName;
	}
	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}
	
	
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String username) {
		this.userName = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	
	
}
