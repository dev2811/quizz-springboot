package com.group2.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.group2.entity.UserEntity;

public class UserLoginResponse {

	@JsonProperty("id")
	private int id;
	
	@JsonProperty("name")
	private String name;
	
	@JsonProperty("role")
	private String role;
	
	@JsonProperty("token")
	private String token;
	
	private int status;
	
	public UserLoginResponse() {
		
	}
	
	public UserLoginResponse(UserEntity entity) {
	this.id = entity.getId();
	this.name = entity.getUserName();
	this.role = entity.getRoleName();
	}

	
	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}
	
	
	
}
