package com.group2.response;

import java.util.List;
import java.util.stream.Collectors;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.group2.entity.QuestionEntity;

public class QuestResponse {
	@JsonProperty("id")
	private int id;

	@JsonProperty("content")
	private String content;

	@JsonProperty("type")
	private int type;
	
	public QuestResponse() {
		
	}

	public QuestResponse(QuestionEntity quest) {
		this.id =quest.getId();
		this.content =quest.getContent();
		this.type = quest.getType();
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}
	
	public List<QuestResponse> mapTopList(List<QuestionEntity> listQuest){
		List<QuestResponse> list = listQuest.stream().map(x->new QuestResponse(x)).collect(Collectors.toList());
	return list;
	}
}
