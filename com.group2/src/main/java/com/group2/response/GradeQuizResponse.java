package com.group2.response;

import java.util.List;

import com.group2.dto.GradeQuizDTO;

public class GradeQuizResponse {
	private List<GradeQuizDTO> gradeQuizes;

	public List<GradeQuizDTO> getGradeQuizes() {
		return gradeQuizes;
	}

	public void setGradeQuizes(List<GradeQuizDTO> gradeQuizes) {
		this.gradeQuizes = gradeQuizes;
	}
	
}
