package com.group2.response;

public class ExtraDataResponse extends BaseResponse {
	private Object extraData;

	public ExtraDataResponse() {
		super();
		this.extraData = null;
	}

	public Object getExtraData() {
		return extraData;
	}

	public void setExtraData(Object extraData) {
		this.extraData = extraData;
	}
}
