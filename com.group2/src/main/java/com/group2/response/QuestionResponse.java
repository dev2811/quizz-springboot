package com.group2.response;

import java.util.List;

import com.group2.entity.QuestionEntity;

public class QuestionResponse {

	private List<QuestionEntity> questions;

	public List<QuestionEntity> getQuestions() {
		return questions;
	}

	public void setQuestions(List<QuestionEntity> list) {
		this.questions = list;
	}
}
