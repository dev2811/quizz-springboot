package com.group2.response;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.group2.entity.AnswerEntity;
import com.group2.entity.QuestionEntity;

public class QuestDetailResponse {

	private int id;
	private String contentQuest;
	private String contentQuest2;
	private String answers;
	private List<String> answersReal;
	private List<String> answersFake;
	private int type;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getContentQuest() {
		return contentQuest;
	}

	public void setContentQuest(String contentQuest) {
		this.contentQuest = contentQuest;
	}

	public String getContentQuest2() {
		return contentQuest2;
	}

	public void setContentQuest2(String contentQuest2) {
		this.contentQuest2 = contentQuest2;
	}

	public String getAnswers() {
		return answers;
	}

	public void setAnswers(String answers) {
		this.answers = answers;
	}

	public List<String> getAnswersReal() {
		return answersReal;
	}

	public void setAnswersReal(List<String> answersReal) {
		this.answersReal = answersReal;
	}

	public List<String> getAnswersFake() {
		return answersFake;
	}

	public void setAnswersFake(List<String> answersFake) {
		this.answersFake = answersFake;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public QuestDetailResponse getQuestBasic(QuestionEntity entity) {
		QuestDetailResponse questResponse = new QuestDetailResponse();
		String content = entity.getContent();

		String[] contents = content.split("____");
		questResponse.setContentQuest(contents[0]);
		if (!questResponse.getContentQuest().equals(entity.getContent())) {
			questResponse.setContentQuest2(contents[1]);
		}
		questResponse.setType(entity.getType());

		if (entity.getType() != 1) {
			List<String> listFake = new ArrayList<String>();
			List<String> listReal = new ArrayList<String>();
			for (AnswerEntity x : entity.getAnswers()) {
				if (x.getCheck()) {
					listReal.add(x.getContent());
				} else {
					listFake.add(x.getContent());
				}
			}
			questResponse.setAnswersFake(listFake);
			questResponse.setAnswersReal(listReal);
		}else {
			questResponse.setAnswers(entity.getAnswers().get(0).getContent());
		}
		
		questResponse.setId(entity.getId());
		
		return questResponse;
	}

}
