package com.group2.response;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class QuestPaginationResponse {

	@JsonProperty("total")
	private int total;

	@JsonProperty("quests")
	private List<QuestResponse> quests;
	
	public QuestPaginationResponse() {
		this.quests = new ArrayList<QuestResponse>();
	}

	public int getTotal() {
		return total;
	}

	public void setTotal(int total) {
		this.total = total;
	}

	public List<QuestResponse> getQuests() {
		return quests;
	}

	public void setQuests(List<QuestResponse> quests) {
		this.quests = quests;
	}
}
