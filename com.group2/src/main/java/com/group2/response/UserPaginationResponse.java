package com.group2.response;

import java.util.List;

import com.group2.entity.UserEntity;

public class UserPaginationResponse {
	private List<UserEntity> users;
	private int totalPages;
	
	public List<UserEntity> getUsers() {
		return users;
	}
	
	public int getTotalPages() {
		return totalPages;
	}
	
	public void setUsers(List<UserEntity> users) {
		this.users = users;
	}
	
	public void setTotalPages(int totalPages) {
		this.totalPages = totalPages;
	}
	
}
