package com.group2.response;

import com.group2.entity.UserEntity;

public class HomeResponse {
	private UserEntity loginedUser;
	private QuizPaginationResponse quizes;
	
	public void setQuizes(QuizPaginationResponse quizes) {
		this.quizes = quizes;
	}

	public QuizPaginationResponse getQuizes() {
		return quizes;
	}

	public UserEntity getLoginedUser() {
		return loginedUser;
	}
	
	public void setLoginedUser(UserEntity loginedUser) {
		this.loginedUser = loginedUser;
	}
	
}
