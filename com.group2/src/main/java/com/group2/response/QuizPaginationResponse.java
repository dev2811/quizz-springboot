package com.group2.response;

import java.util.List;

import com.group2.entity.QuizEntity;

public class QuizPaginationResponse {
	private List<QuizEntity> quizes;
	private int totalPages;
	
	public List<QuizEntity> getQuizes() {
		return quizes;
	}
	
	public int getTotalPages() {
		return totalPages;
	}
	
	public void setQuizes(List<QuizEntity> quizes) {
		this.quizes = quizes;
	}
	
	public void setTotalPages(int totalPages) {
		this.totalPages = totalPages;
	}
}
