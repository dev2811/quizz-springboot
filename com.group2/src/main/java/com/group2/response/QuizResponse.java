package com.group2.response;

import java.util.List;

import com.group2.entity.QuizEntity;

public class QuizResponse {
	private List<QuizEntity> quizes;

	public List<QuizEntity> getQuizes() {
		return quizes;
	}

	public void setQuizes(List<QuizEntity> quizes) {
		this.quizes = quizes;
	}
}
