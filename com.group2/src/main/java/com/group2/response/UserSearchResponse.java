package com.group2.response;

public class UserSearchResponse {
	private int id;
	private String userName;
	private String email;
	private int status;
	
	public int getId() {
		return id;
	}
	
	public String getUserName() {
		return userName;
	}
	
	public String getEmail() {
		return email;
	}
	
	public int getStatus() {
		return status;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public void setStatus(int status) {
		this.status = status;
	}
}
