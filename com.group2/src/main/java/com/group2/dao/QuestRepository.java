package com.group2.dao;

import java.util.List;


import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.group2.entity.QuestionEntity;

public interface QuestRepository extends JpaRepository<QuestionEntity, Integer>{

	QuestionEntity findByContent(String content);
	
	QuestionEntity findById(int id);
	
	List<QuestionEntity> findByQuizId(int id);
	
	long countByQuizId(int id);

	Page<QuestionEntity> findByQuizId(int quizId,Pageable pageable);
	
	List<QuestionEntity> findByQuizIdAndContentLike(int quizId,String content);
}
