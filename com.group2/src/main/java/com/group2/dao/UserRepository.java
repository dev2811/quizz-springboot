package com.group2.dao;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.group2.entity.UserEntity;

@Repository
public interface UserRepository extends JpaRepository<UserEntity, Integer>{

	UserEntity findByUserNameAndPassword(String userName,String password);
	
	UserEntity findByUserName(String userName);
	
	UserEntity findByEmail(String email);
	
	List<UserEntity> findByStatus(Integer status,Sort sort);
	
	List<UserEntity> findByStatus(Integer status, Pageable pageable);
	
	List<UserEntity> findByStatus(Integer status);
	
	List<UserEntity> findByStatusAndRoleNameEquals(Integer status, String role);
	
	List<UserEntity> findByUserNameContainingIgnoreCase(String username);
	
	List<UserEntity> findByUserNameContainingIgnoreCaseAndRoleNameEquals(String username, String role);
	
	Page<UserEntity> findByRoleNameEquals(String role, Pageable pageable);
}
