package com.group2.dao;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import com.group2.entity.AnswerEntity;

public interface AnswerRepository extends JpaRepository<AnswerEntity, Integer> {

	@Query(value = "delete from dbo.Answer where dbo.Answer.question_id=?1", nativeQuery = true)
	@Transactional
	@Modifying
	public int deleteByQuestIdCustom(int id);
	
	public List<AnswerEntity> getByQuestId(int id);
	
	@Query(value = "select * from dbo.Answer where checkQ=1 and question_id=?1", nativeQuery = true)
	public List<AnswerEntity> findByQuestId(int id);
}
