package com.group2.dao;


import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.group2.entity.QuizEntity;

@Repository
public interface QuizRepository extends JpaRepository<QuizEntity, Integer>{
	
	QuizEntity findById(int id);
	
	QuizEntity findByName(String name);
	
	List<QuizEntity> findByNameContainingIgnoreCase(String name);
	
	String findNameById(int id);
}
