package com.group2.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.group2.dto.GradeQuizDTO;
import com.group2.entity.GradeEntity;


@Repository
public interface GradeQuizRepository extends JpaRepository<GradeEntity, Integer>{
	List<GradeEntity> findByUserUserNameEquals(String username);
	
	@Query("SELECT new com.group2.dto.GradeQuizDTO(MAX(g.score),g.quiz) FROM GradeEntity g WHERE g.user.userName = :username GROUP BY g.quiz")
	List<GradeQuizDTO> findGradeQuizOfUser(@Param("username") String username);
	
	GradeEntity findOneByQuizId(int id);
	
}
