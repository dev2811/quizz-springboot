package com.group2.service;

import org.springframework.stereotype.Service;
import com.group2.config.EmailConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.Properties;

@Service
public class EmailService {
	private static final String CONTENT_TYPE_TEXT_HTML = "text/html;charset=\"utf-8\"";

	@Value("${spring.mail.host}")
	private String host;
	@Value("${spring.mail.port}")
	private String port;
	@Value("${spring.mail.username}")
	private String email;
	@Value("${spring.mail.password}")
	private String password;

	public EmailConfig emailConfig = new EmailConfig();

	public void sendMailConfirm(String emailReceived, String urlConfirm, String userName) {
		Properties props = new Properties();
		props.put("mail.smtp.host", "smtp.gmail.com");
		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.port", "587");

		Session session = Session.getInstance(props, new Authenticator() {
			@Override
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication("letienhoa010198@gmail.com", "Letienhoadtq2811");
			}
		});
		Message message = new MimeMessage(session);
		try {
			message.setRecipients(Message.RecipientType.TO,
					new InternetAddress[] { new InternetAddress("letienhoa27121997@gmail.com") });
			message.setFrom(new InternetAddress("letienhoa010198@gmail.com"));
			message.setSubject("Confirm Email");
			message.setContent(emailConfig.getContentForConfirmAccount(userName, urlConfirm), CONTENT_TYPE_TEXT_HTML);
			Transport.send(message);
		} catch (MessagingException e) {
			e.printStackTrace();
		}
	}
}
