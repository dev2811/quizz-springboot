package com.group2.service;

import com.group2.response.FacebookUserResponse;

public interface SocialLoginService {
	public FacebookUserResponse getLoginedFacebookUser(String facebookToken);
}
