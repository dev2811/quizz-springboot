package com.group2.service;

import org.springframework.data.domain.Pageable;

import com.group2.request.QuestRequest;
import com.group2.request.QuestionListRequest;
import com.group2.request.UpdateQuestRequest;
import com.group2.response.QuestionResponse;
import com.group2.response.ResultResponse;
import com.group2.response.TotalAndNameResponse;
import com.group2.response.QuestDetailResponse;
import com.group2.response.QuestPaginationResponse;

public interface QuestService {
	
	public boolean createQuest(QuestRequest quest);
	
	public boolean updateQuest(UpdateQuestRequest quest);
	
	public QuestionResponse getAllQuestFromQuiz(int id);
	
	public TotalAndNameResponse getTotalQuestAndQuizName(int id);
	
	public ResultResponse getResultForQuiz(QuestionListRequest questions, int quizId, String userName);

	public QuestDetailResponse findById(int id);
	
	public QuestPaginationResponse getListQuestPagination(int quizId,Pageable pageable);
	
	public boolean deleteQuest(int id);
	
	public QuestPaginationResponse getListQuestPaginationByName(int quizId,String name);
}
