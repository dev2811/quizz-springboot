package com.group2.service;

import java.util.List;

import org.springframework.data.domain.Pageable;

import com.group2.entity.UserEntity;
import com.group2.request.UserRequest;
import com.group2.response.UserLoginResponse;
import com.group2.response.UserPaginationResponse;
import com.group2.response.UserResponse;
import com.group2.response.UserSearchResponse;

public interface UserService {

	UserLoginResponse checkLogin(String username,String password);
	
	UserEntity findUserByUserName(String username);
	
	UserEntity getUserForAuthen(String username);
	
	UserResponse create(UserRequest userRequest);
	
	List<UserResponse> getListAllForRank();
	
	List<UserResponse> rank(Pageable pageable);
	
	UserPaginationResponse getAllUsers(int pageNumber);
	
	List<UserSearchResponse> getUserByUsername(String username);
	
	List<UserSearchResponse> getUserByStatus(Integer status);
	
	UserSearchResponse disableUser(int id);
	
	UserSearchResponse activateUser(int id);

	boolean enableUser(String userName);
	
	UserLoginResponse checkLoginGoogle(UserRequest userRequest);
}
