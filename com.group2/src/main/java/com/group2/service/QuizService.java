package com.group2.service;

import com.group2.entity.QuizEntity;
import com.group2.request.QuizRequest;
import com.group2.response.QuizPaginationResponse;
import com.group2.response.QuizResponse;

public interface QuizService {
	QuizPaginationResponse getAllQuiz(int pageNumber);
	
	QuizResponse getQuizByName(String name);
	
	QuizResponse getQuizById(int id);
	
	boolean deleteQuiz(int id);
	
	//Page<QuizEntity> getAllQuiz(int pageNumber);
	
	QuizEntity findQuizByName(String quizName);
	
	boolean addQuiz(QuizRequest wrapper);
}
