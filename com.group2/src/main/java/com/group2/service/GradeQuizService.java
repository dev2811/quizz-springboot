package com.group2.service;

import java.util.List;

import com.group2.response.GradeQuizResponse;

public interface GradeQuizService {
	GradeQuizResponse getAllGradeQuizOfUser(String username);
}
