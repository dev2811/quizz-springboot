package com.group2.request;

import com.fasterxml.jackson.annotation.JsonProperty;

public class UserLoginRequest {

	@JsonProperty("user_name")
	private String username;
	
	@JsonProperty("password")
	private String password;

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
}
