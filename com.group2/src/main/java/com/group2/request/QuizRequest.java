package com.group2.request;

import com.fasterxml.jackson.annotation.JsonProperty;

public class QuizRequest {
	
	@JsonProperty("quiz_name")
	private String quizName;
	@JsonProperty("tag_name")
	private String tagName;
	
	@JsonProperty("image")
	private String image;

	public String getQuizName() {
		return quizName;
	}

	public void setQuizName(String quizName) {
		this.quizName = quizName;
	}

	public String getTagName() {
		return tagName;
	}

	public void setTagName(String tagName) {
		this.tagName = tagName;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}
	
}
