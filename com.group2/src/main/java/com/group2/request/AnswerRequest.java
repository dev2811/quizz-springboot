package com.group2.request;

import com.fasterxml.jackson.annotation.JsonProperty;

public class AnswerRequest {
	
	@JsonProperty("id")
	private int id;
	
	@JsonProperty("content")
	private String content;
	
	@JsonProperty("check")
	private boolean check;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	

	public boolean getCheck() {
		return check;
	}

	public void setCheck(boolean check) {
		this.check = check;
	}
}
