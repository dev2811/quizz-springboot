package com.group2.request;

import java.util.ArrayList;
import java.util.List;

public class QuestionListRequest {
	private List<QuestionRequest> questions = new ArrayList<QuestionRequest>();

	public List<QuestionRequest> getQuestions() {
		return questions;
	}

	public void setQuestions(List<QuestionRequest> questions) {
		this.questions = questions;
	}
}
