package com.group2.controller;

import java.security.Principal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.group2.response.BaseResponse;
import com.group2.response.GradeQuizResponse;
import com.group2.service.GradeQuizService;

@RestController
@CrossOrigin(maxAge = 3600)
@RequestMapping("api/gradequiz")
public class GradeQuizController {
	@Autowired
	private GradeQuizService gradeQuizService;
	
	@RequestMapping(value = "/get-grade-quiz-of-user")
	public ResponseEntity<BaseResponse> getAllGradeQuizOfUser(Principal principal){
		BaseResponse response = new BaseResponse();
		
		String username = principal.getName();
		
		GradeQuizResponse gradeQuizResponse = gradeQuizService.getAllGradeQuizOfUser(username);
		response.setData(gradeQuizResponse);
		
		return new ResponseEntity<BaseResponse>(response, HttpStatus.OK);
	}
}
