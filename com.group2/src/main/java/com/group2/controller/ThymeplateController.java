package com.group2.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.group2.config.LinksUrl;
import com.group2.config.Utils;
import com.group2.response.BaseResponse;
import com.group2.service.UserService;
import com.group2.thread.EmailThread;

@Controller
@CrossOrigin(maxAge = 3600)
@RequestMapping(value = "api/confirm")
public class ThymeplateController {
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private LinksUrl linksUrl;
	
	@RequestMapping(value = "/check", method = RequestMethod.GET)
	public ResponseEntity<BaseResponse> check() {
		BaseResponse response = new BaseResponse();
		sendMail("", linksUrl.getLinksUrlSpringboot() + "api/confirm/email?user-name=hoa1234", "HoaLT20");
		return new ResponseEntity<BaseResponse>(response, HttpStatus.OK);
	}

	@GetMapping(value = "/email")
	public String confirm(Model model,
			@RequestParam(name = "user-name", required = false, defaultValue = "") String userName,
			@RequestParam(name = "code", required = false, defaultValue = "") String code) {
		model.addAttribute("linkLogin", linksUrl.getLinksUrlSpringMvc());
		if(Utils.codeMail.contains(code)) {
			if (userService.enableUser(userName)) {
				Utils.codeMail.remove(code);
				return "ConfirmSuccess";
			}
		}
		return "ConfirmError";
	}
	
	public void sendMail(String emailReceived, String urlConfirm, String userName) {
		Thread threadMail = new Thread(new EmailThread(emailReceived, urlConfirm, userName));
		threadMail.start();
	}
}
