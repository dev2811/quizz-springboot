package com.group2.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.group2.entity.ResponseStatusEnum;
import com.group2.response.BaseResponse;
import com.group2.response.FacebookUserResponse;
import com.group2.service.SocialLoginService;

@RestController
@CrossOrigin(maxAge = 3600)
@RequestMapping(value = "api/oauth")
public class SocialLoginController {
	@Autowired
	private SocialLoginService socialLoginService;
	
	@RequestMapping(value = "/facebook", method=RequestMethod.POST, consumes=MediaType.APPLICATION_FORM_URLENCODED_VALUE)
	public ResponseEntity<BaseResponse> getLoginedFacebookUser(@RequestBody MultiValueMap<String, String> request){
		BaseResponse response = new BaseResponse();
		
		System.out.println(request.get("facebookToken").get(0));
		FacebookUserResponse facebookUser = socialLoginService.getLoginedFacebookUser(request.get("facebookToken").get(0));
		System.out.println(facebookUser);
		
		if(facebookUser!=null) {
			facebookUser.setId("0");
			response.setData(facebookUser);
		}
		else {
			response.setStatus(ResponseStatusEnum.DATA_INVALID);
			response.setMessageError("This facebook account has something wrong!");
		}
		
		return new ResponseEntity<BaseResponse>(response, HttpStatus.OK);
	}
}
