package com.group2.controller;

import java.awt.PageAttributes.MediaType;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.group2.entity.ResponseStatusEnum;
import com.group2.request.QuizRequest;
import com.group2.response.BaseResponse;
import com.group2.response.QuestPaginationResponse;
import com.group2.service.QuestService;
import com.group2.service.QuizService;

@RestController
@CrossOrigin(maxAge = 3600)
@RequestMapping(value = "api/quiz")
public class QuizController {

	@Autowired
	private QuizService quizSevice;

	@Autowired
	private QuestService questService;

	@RequestMapping(method = RequestMethod.POST, path = "/add")
	public ResponseEntity<BaseResponse> addQuiz(@RequestBody QuizRequest wrapper) {
		BaseResponse response = new BaseResponse();
		if (quizSevice.findQuizByName(wrapper.getQuizName()) != null) {
			response.setStatus(ResponseStatusEnum.DATA_INVALID);
			response.setMessageError("Quiz is already");
		} else if (!quizSevice.addQuiz(wrapper)) {
			response.setStatus(ResponseStatusEnum.FAIL);
			response.setMessageError("Add error");
		}
		return new ResponseEntity<BaseResponse>(response, HttpStatus.OK);
	}

	@RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<BaseResponse> deleteQuiz(@PathVariable int id) {
		BaseResponse response = new BaseResponse();
		boolean check = quizSevice.deleteQuiz(id);
		if (!check) {
			response.setStatus(ResponseStatusEnum.NOT_FOUND);
			response.setMessage(ResponseStatusEnum.NOT_FOUND);
		}
		return new ResponseEntity<BaseResponse>(response, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, path = "/{id}/get-quests")
	public ResponseEntity<BaseResponse> getQuestByPagination(@PathVariable("id") int quizId,
			@RequestParam(name = "page-number", required = false, defaultValue = "0") int page,
			@RequestParam(name = "page-limit", required = false, defaultValue = "6") int limit) {
		BaseResponse response = new BaseResponse();
		QuestPaginationResponse listQuest = questService.getListQuestPagination(quizId, PageRequest.of(page, limit));
		if (listQuest == null) {
			response.setStatus(ResponseStatusEnum.FAIL);
			response.setMessageError("Add quest failed");
		}
		response.setData(listQuest);
		return new ResponseEntity<BaseResponse>(response, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, path = "/{id}/get-quests-by-name")
	public ResponseEntity<BaseResponse> getQuestByName(@PathVariable("id") int quizId,
			@RequestParam(name = "page-number", required = false, defaultValue = "0") int page,
			@RequestParam(name = "text-search", required = false, defaultValue = "") String content,
			@RequestParam(name = "page-limit", required = false, defaultValue = "100") int limit) {
		BaseResponse response = new BaseResponse();
		QuestPaginationResponse listQuest = questService.getListQuestPaginationByName(quizId, content);
		if (listQuest == null) {
			response.setStatus(ResponseStatusEnum.FAIL);
			response.setMessageError("Add quest failed");
		}
		response.setData(listQuest);
		return new ResponseEntity<BaseResponse>(response, HttpStatus.OK);
	}
}
