package com.group2.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.group2.entity.UserEntity;
import com.group2.entity.ResponseStatusEnum;
import com.group2.request.UserLoginRequest;
import com.group2.request.UserRequest;
import com.group2.response.BaseResponse;
import com.group2.response.QuizResponse;
import com.group2.response.UserLoginResponse;
import com.group2.response.UserPaginationResponse;
import com.group2.response.UserResponse;
import com.group2.response.UserSearchResponse;
import com.group2.service.UserService;
import com.group2.thread.EmailThread;

@RestController
@CrossOrigin(maxAge = 3600)
@RequestMapping(value = "api/user")
public class UserController {

	@Autowired
	private UserService userService;

	/*
	 * @RequestMapping(value = "/login", method = RequestMethod.POST) public
	 * ResponseEntity<BaseResponse> login(HttpServletRequest request, @RequestBody
	 * UserLoginRequest wrapper) { BaseResponse response = new BaseResponse();
	 * UserLoginResponse userResponse
	 * =userService.checkLogin(wrapper.getUsername(),wrapper.getPassword());
	 * if(userResponse.getStatus()==1) { response.setData(userResponse); return new
	 * ResponseEntity<BaseResponse>(response, HttpStatus.OK); }else
	 * if(userResponse.getStatus()==0) {
	 * response.setMessageError("Account has been disabled."); return new
	 * ResponseEntity<BaseResponse>(response, HttpStatus.NOT_FOUND); }else {
	 * response.setMessageError("Incorrect account or password."); return new
	 * ResponseEntity<BaseResponse>(response, HttpStatus.NOT_FOUND); } }
	 */

	@RequestMapping(value = "/login", method = RequestMethod.POST)
	public ResponseEntity<BaseResponse> login(HttpServletRequest request, @RequestBody UserLoginRequest wrapper) {
		BaseResponse response = new BaseResponse();
		if (wrapper.getUsername() == null || wrapper.getPassword() == null) {
			response.setStatus(ResponseStatusEnum.PARAMS_INVALID);
			response.setMessageError("Incorrect account or password.");
			return new ResponseEntity<BaseResponse>(response, HttpStatus.OK);
		}

		UserLoginResponse userResponse = userService.checkLogin(wrapper.getUsername(), wrapper.getPassword());
		if (userResponse.getStatus() == 1) {
			response.setData(userResponse);
		} else if (userResponse.getStatus() == 0) {
			response.setStatus(ResponseStatusEnum.PARAMS_INVALID);
			response.setMessageError("Account has been disabled.");
		} else {
			response.setStatus(ResponseStatusEnum.PARAMS_INVALID);
			response.setMessageError("Incorrect account or password.");
		}
		return new ResponseEntity<BaseResponse>(response, HttpStatus.OK);
	}

	@RequestMapping(value = "/register", method = RequestMethod.POST)
	public ResponseEntity<BaseResponse> register(HttpServletRequest request, @RequestBody UserRequest user) {
		BaseResponse response = new BaseResponse();
		UserResponse userResponse = userService.create(user);
		if (userResponse.getStatus() == 1) {
			response.setData(userResponse);
			response.setMessage(ResponseStatusEnum.SUCCESS);
		}
		if (userResponse.getStatus() == 2) {
			response.setStatus(ResponseStatusEnum.PARAMS_INVALID);
			response.setMessage(ResponseStatusEnum.PARAMS_INVALID);
		}
		if (userResponse.getStatus() == 3) {
			response.setStatus(ResponseStatusEnum.NOT_FOUND);
			response.setMessageError("Tên tài khoản đã tồn tại");
		}
		if (userResponse.getStatus() == 4) {
			response.setStatus(ResponseStatusEnum.FAIL);
			response.setMessage(ResponseStatusEnum.FAIL);
		}
		return new ResponseEntity<BaseResponse>(response, HttpStatus.OK);
	}

	@RequestMapping(value = "/listOfAllForRank", method = RequestMethod.GET)
	public ResponseEntity<BaseResponse> listAllForRank() {
		BaseResponse response = new BaseResponse();
		List<UserResponse> userResponse = userService.getListAllForRank();
		if (userResponse != null) {
			response.setData(userResponse);
			response.setMessage(ResponseStatusEnum.SUCCESS);
		}
		if (userResponse == null) {
			response.setStatus(ResponseStatusEnum.FAIL);
			response.setMessage(ResponseStatusEnum.FAIL);
		}
		return new ResponseEntity<BaseResponse>(response, HttpStatus.OK);
	}

	@RequestMapping(value = "/rank", method = RequestMethod.GET)
	public ResponseEntity<BaseResponse> rankPageable(@RequestParam(defaultValue = "0") int page) {
		BaseResponse response = new BaseResponse();
		Pageable pageable = PageRequest.of(page, 10, Sort.by("currentScores").descending());
		List<UserResponse> userResponse = userService.rank(pageable);
		if (userResponse != null) {
			response.setData(userResponse);
			response.setMessage(ResponseStatusEnum.SUCCESS);
		}
		if (userResponse == null) {
			response.setStatus(ResponseStatusEnum.FAIL);
			response.setMessage(ResponseStatusEnum.FAIL);
		}
		return new ResponseEntity<BaseResponse>(response, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/get-all-users", method = RequestMethod.GET)
	public ResponseEntity<BaseResponse> getAllUsers(@RequestParam(defaultValue = "1") int pageNumber){
		BaseResponse response = new BaseResponse();
		
		UserPaginationResponse userResponse = userService.getAllUsers(pageNumber);
		response.setData(userResponse);
		
		return new ResponseEntity<BaseResponse>(response, HttpStatus.OK); 
	}
	
	@RequestMapping(value="/get-user-by-name", method=RequestMethod.GET)
	public ResponseEntity<BaseResponse> getUserByName(@RequestParam(defaultValue = "") String username){
		BaseResponse response = new BaseResponse();
		
		List<UserSearchResponse> userResponse = userService.getUserByUsername(username);
		response.setData(userResponse);
		
		return new ResponseEntity<BaseResponse>(response, HttpStatus.OK);
	}
	
	@RequestMapping(value="/get-user-by-status", method=RequestMethod.GET)
	public ResponseEntity<BaseResponse> getUserByName(@RequestParam(defaultValue = "-1") int status){
		BaseResponse response = new BaseResponse();
		
		List<UserSearchResponse> userResponse = userService.getUserByStatus(status);
		response.setData(userResponse);
		
		return new ResponseEntity<BaseResponse>(response, HttpStatus.OK);
	}
	
	@RequestMapping(value="/disable-user/{id}", method=RequestMethod.GET)
	public ResponseEntity<BaseResponse> disableUser(@PathVariable int id){
		BaseResponse response = new BaseResponse();
		
		UserSearchResponse userResponse = userService.disableUser(id);
		response.setData(userResponse);
		
		return new ResponseEntity<BaseResponse>(response, HttpStatus.OK);
	}
	
	@RequestMapping(value="/activate-user/{id}", method=RequestMethod.GET)
	public ResponseEntity<BaseResponse> activateUser(@PathVariable int id){
		BaseResponse response = new BaseResponse();
		
		UserSearchResponse userResponse = userService.activateUser(id);
		response.setData(userResponse);
		
		return new ResponseEntity<BaseResponse>(response, HttpStatus.OK);
	}

	@RequestMapping(value = "/login-google", method = RequestMethod.POST)
	public ResponseEntity<BaseResponse> loginGoogle(HttpServletRequest request, @RequestBody UserRequest wrapper) {
		BaseResponse response = new BaseResponse();	
		UserLoginResponse userResponse =new UserLoginResponse();
		userResponse = userService.checkLoginGoogle(wrapper);
		 if(userResponse.getStatus()==0) {
			response.setStatus(ResponseStatusEnum.PARAMS_INVALID);
			response.setMessageError("Incorrect account or password.");
		}
		 response.setData(userResponse);
		return new ResponseEntity<BaseResponse>(response, HttpStatus.OK);
	}
}
