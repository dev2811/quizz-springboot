package com.group2.controller;

import java.security.Principal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.group2.config.LinksUrl;
import com.group2.entity.QuizEntity;
import com.group2.entity.UserEntity;
import com.group2.response.BaseResponse;
import com.group2.response.QuizPaginationResponse;
import com.group2.response.QuizResponse;
import com.group2.service.EmailService;
import com.group2.service.QuizService;
import com.group2.service.UserService;

@RestController
@CrossOrigin(maxAge = 3600)
@RequestMapping(value = "api/home")
public class HomeController {
	@Autowired
	private QuizService quizService;

	@Autowired
	private UserService userService;

	@Autowired
	private EmailService emailService;

	@RequestMapping(method = RequestMethod.GET, path = "/")
	public ResponseEntity<BaseResponse> home(@RequestParam(defaultValue = "1") int pageNumber) {
		BaseResponse response = new BaseResponse();
		// HomeResponse homeRes = new HomeResponse();

		// UserEntity loginedUser = userService.getUserForAuthen(principal.getName());
		QuizPaginationResponse quizes = quizService.getAllQuiz(pageNumber);

		// homeRes.setLoginedUser(loginedUser);
		// homeRes.setQuizes(quizes);

		response.setData(quizes);

		return new ResponseEntity<BaseResponse>(response, HttpStatus.OK);
	}

	@RequestMapping(value = "/get-quiz-by-name", method = RequestMethod.GET)
	public ResponseEntity<BaseResponse> getQuizByName(@RequestParam(defaultValue = "") String name) {
		BaseResponse response = new BaseResponse();

		QuizResponse quizSearch = quizService.getQuizByName(name);
		response.setData(quizSearch);

		return new ResponseEntity<BaseResponse>(response, HttpStatus.OK);
	}


	@RequestMapping(value = "/get-logined-user", method = RequestMethod.GET)
	public ResponseEntity<BaseResponse> getLoginedUser(Principal principal) {
		BaseResponse response = new BaseResponse();

		UserEntity loginedUser = userService.getUserForAuthen(principal.getName());
		response.setData(loginedUser);

		return new ResponseEntity<BaseResponse>(response, HttpStatus.OK);
	}

}
