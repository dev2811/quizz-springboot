package com.group2.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.group2.entity.QuestionEntity;
import com.group2.entity.ResponseStatusEnum;
import com.group2.request.QuestRequest;
import com.group2.request.QuestionListRequest;
import com.group2.request.UpdateQuestRequest;
import com.group2.response.BaseResponse;
import com.group2.response.QuestionResponse;
import com.group2.response.ResultResponse;
import com.group2.response.TotalAndNameResponse;
import com.group2.response.QuestDetailResponse;
import com.group2.service.QuestService;

@RestController
@CrossOrigin(maxAge = 3600)
@RequestMapping(value = "api/quest")
public class QuestionController {

	@Autowired
	private QuestService questService;

	@RequestMapping(method = RequestMethod.POST, path = "/add")
	public ResponseEntity<BaseResponse> addQuest(@RequestBody QuestRequest wrapper) {
		BaseResponse response = new BaseResponse();
		if (!questService.createQuest(wrapper)) {
			response.setStatus(ResponseStatusEnum.FAIL);
			response.setMessageError("Add quest failed");
		}
		return new ResponseEntity<BaseResponse>(response, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.POST, path = "/update")
	public ResponseEntity<BaseResponse> updateQuest(@RequestBody UpdateQuestRequest wrapper) {
		BaseResponse response = new BaseResponse();
		if (!questService.updateQuest(wrapper)) {
			response.setStatus(ResponseStatusEnum.FAIL);
			response.setMessageError("Add quest failed");
		}
		return new ResponseEntity<BaseResponse>(response, HttpStatus.OK);
	}
	
	@RequestMapping(value="/get-question-by-quizId", method=RequestMethod.GET)
	public ResponseEntity<BaseResponse> getQuizById(@RequestParam(defaultValue = "0") Integer id){
		BaseResponse response = new BaseResponse();
		QuestionResponse questions = questService.getAllQuestFromQuiz(id);
		if(questions==null) {
			response.setStatus(ResponseStatusEnum.FAIL);
			response.setMessageError("Sai dữ liệu");
		}else {
			response.setData(questions);
		}
		return new ResponseEntity<BaseResponse>(response, HttpStatus.OK);
	}
	
	@RequestMapping(value="/get-total-question-and-quiz-name", method=RequestMethod.GET)
	public ResponseEntity<BaseResponse> getTotalQuestionById(@RequestParam(defaultValue = "0") Integer quizId){
		BaseResponse response = new BaseResponse();
		TotalAndNameResponse questions = questService.getTotalQuestAndQuizName(quizId);
		if(questions==null) {
			response.setStatus(ResponseStatusEnum.FAIL);
			response.setMessageError("Sai dữ liệu");
		}else {
			response.setData(questions);
		}
		return new ResponseEntity<BaseResponse>(response, HttpStatus.OK);
	}
	
	@RequestMapping(value="/result", method=RequestMethod.POST)
	public ResponseEntity<BaseResponse> getResult(@RequestBody QuestionListRequest questions, @RequestParam(defaultValue = "0") Integer quizId, @RequestParam(defaultValue = "") String username){
		BaseResponse response = new BaseResponse();
		ResultResponse result = questService.getResultForQuiz(questions, quizId, username);
		if(result==null) {
			response.setStatus(ResponseStatusEnum.FAIL);
			response.setMessageError("Sai dữ liệu");
		}else {
			response.setData(result);
		}
		return new ResponseEntity<BaseResponse>(response, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, path = "/{id}/detail")
	public ResponseEntity<BaseResponse> getDetailQuest(
			@PathVariable("id") int questId) {
		BaseResponse response = new BaseResponse();
		QuestDetailResponse quest = questService.findById(questId);
		response.setData(quest);
		return new ResponseEntity<BaseResponse>(response, HttpStatus.OK);
	}
	
	@RequestMapping(method = RequestMethod.GET, path = "/{id}/delete")
	public ResponseEntity<BaseResponse> deleteQuest(@PathVariable(name="id") int questId) {
		BaseResponse response = new BaseResponse();
		if (!questService.deleteQuest(questId)) {
			response.setStatus(ResponseStatusEnum.FAIL);
			response.setMessageError("Delete quest failed");
		}
		return new ResponseEntity<BaseResponse>(response, HttpStatus.OK);
	}
}
