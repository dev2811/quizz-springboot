package com.group2.thread;

import org.springframework.beans.factory.annotation.Autowired;

import com.group2.service.EmailService;

public class EmailThread implements Runnable {

	private EmailService emailService = new EmailService();
	
	private volatile String emailReceived;
	private volatile String  urlConfirm;
	private volatile String  userName;
	
	public EmailThread(String emailReceived,String urlConfirm,String userName) {
		this.emailReceived = emailReceived;
		this.urlConfirm = urlConfirm;
		this.userName = userName;
	}
	
	@Override
	public void run() {
		emailService.sendMailConfirm(this.emailReceived,this.urlConfirm,this.userName);
	}

	
}
