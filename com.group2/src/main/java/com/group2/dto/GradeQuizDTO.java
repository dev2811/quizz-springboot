package com.group2.dto;

import com.group2.entity.QuizEntity;

public class GradeQuizDTO {
	private int id;
	private double score;
	private QuizEntity quiz;
	
	public GradeQuizDTO() {
		super();
	}

	public GradeQuizDTO(double score, QuizEntity quiz) {
		super();
		this.score = score;
		this.quiz = quiz;
	}

	public GradeQuizDTO(int id, double score, QuizEntity quiz) {
		super();
		this.id = id;
		this.score = score;
		this.quiz = quiz;
	}
	
	public GradeQuizDTO(int id, double score) {
		super();
		this.id = id;
		this.score = score;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	public void setScore(double score) {
		this.score = score;
	}
	
	public void setQuizes(QuizEntity quiz) {
		this.quiz = quiz;
	}
	
	public int getId() {
		return id;
	}
	
	public double getScore() {
		return score;
	}
	
	public QuizEntity getQuiz() {
		return quiz;
	}

	@Override
	public String toString() {
		return "GradeQuizDTO [id=" + id + ", score=" + score + ", quiz=" + quiz + "]";
	}
	
	
}
