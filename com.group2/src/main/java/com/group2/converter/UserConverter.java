package com.group2.converter;

import org.springframework.stereotype.Component;

import com.group2.entity.UserEntity;
import com.group2.request.UserRequest;
import com.group2.response.UserResponse;

@Component
public class UserConverter {
	public UserRequest toRequest(UserEntity user) {
		UserRequest result = new UserRequest();
		result.setFirstName(user.getFirstName());
		result.setLastName(user.getLastName());
		result.setUserName(user.getUserName());
		result.setPassword(user.getPassword());
		result.setEmail(user.getEmail());
		result.setCurrentScores(user.getCurrentScores());
		result.setPreviousScores(user.getPreviousScores());
		result.setStatus(user.getStatus());
		result.setRoleName(user.getRoleName());
		return result;
	}
	
	public UserResponse toResponse(UserEntity user) {
		UserResponse result = new UserResponse();
		result.setFirstName(user.getFirstName());
		result.setLastName(user.getLastName());
		result.setUserName(user.getUserName());
		result.setPassword(user.getPassword());
		result.setEmail(user.getEmail());
		result.setCurrentScores(user.getCurrentScores());
		result.setPreviousScores(user.getPreviousScores());
		result.setStatus(user.getStatus());
		result.setRoleName(user.getRoleName());
		return result;
	}
	
	public UserEntity toEntity(UserRequest user) {
		UserEntity result = new UserEntity();
		result.setFirstName(user.getFirstName());
		result.setLastName(user.getLastName());
		result.setUserName(user.getUserName());
		result.setPassword(user.getPassword());
		result.setEmail(user.getEmail());
		result.setCurrentScores(user.getCurrentScores());
		result.setPreviousScores(user.getPreviousScores());
		result.setStatus(user.getStatus());
		result.setRoleName(user.getRoleName());
		return result;
	}
	
}
