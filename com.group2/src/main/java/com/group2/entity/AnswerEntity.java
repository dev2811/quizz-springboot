package com.group2.entity;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name="answer")
public class AnswerEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private int id;	
	
	@Column(name = "content")
	private String content;
	
	@JsonIgnore
	@Column(name = "checkQ")
	private Boolean check;
	
	@JsonIgnore
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "question_id", nullable = false)
	private QuestionEntity quest;

	public AnswerEntity() {
		
	}
	
	public AnswerEntity(String answer,QuestionEntity quest,boolean check) {
		this.content = answer;
		this.quest =quest;
		this.check = check;
	}
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Boolean getCheck() {
		return check;
	}

	public void setCheck(Boolean check) {
		this.check = check;
	}

	public QuestionEntity getQuest() {
		return quest;
	}

	public void setQuest(QuestionEntity quest) {
		this.quest = quest;
	}
	
	public List<AnswerEntity> mapListToSave(List<String> listAnswer,boolean check,QuestionEntity quest){
		List<AnswerEntity> listReturn = new ArrayList<AnswerEntity>();
		listReturn = listAnswer.stream().map(x->new AnswerEntity(x, quest, check)).collect(Collectors.toList());
		return listReturn;
	}
}
