package com.group2.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.CreationTimestamp;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name="Quiz")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class QuizEntity {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private int id;
	@Column(name = "name")
	private String name;
	@Column(name = "status")
	private int status;
	@Column(name = "tag")
	private String tag;
	@Column(name = "image")
	private String image;
	
	@JsonIgnore
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "user_id", nullable = false)
	private UserEntity user;

	@JsonIgnore
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "updated_date")
	@CreationTimestamp
	private Date updatedDate;
	
	@JsonIgnore
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_date")
	@CreationTimestamp
	private Date createdDate;
	

	
	public void setId(int id) {
		this.id = id;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public void setTag(String tag) {
		this.tag = tag;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public void setUser(UserEntity user) {
		this.user = user;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public int getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public int getStatus() {
		return status;
	}

	public String getTag() {
		return tag;
	}

	public String getImage() {
		return image;
	}

	public UserEntity getUser() {
		return user;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	@Override
	public String toString() {
		return "QuizEntity [id=" + id + ", name=" + name + ", status=" + status + ", tag=" + tag + ", image=" + image
				+ ", user=" + user + ", updatedDate=" + updatedDate + ", createdDate=" + createdDate + "]";
	}
}
