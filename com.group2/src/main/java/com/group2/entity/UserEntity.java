package com.group2.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.CreationTimestamp;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.group2.request.UserRequest;

@Entity
@Table(name = "quizuser")
public class UserEntity {

	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	@Column(name = "first_name")
	private String firstName;
	@Column(name = "last_name")
	private String lastName;
	@Column(name = "username")
	private String userName;
	@Column(name = "password")
	private String password;
	@Column(name = "email")
	private String email;
	@Column(name = "previous_scores")
	private Double previousScores;
	@Column(name = "current_scores")
	private Double currentScores;
	@Column(name = "status")
	private int status;
	@Column(name = "role_name")
	private String roleName;

	@JsonIgnore
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "updated_date")
	@CreationTimestamp
	private Date updatedDate;

	@JsonIgnore
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_date")
	@CreationTimestamp
	private Date createdDate;

	public UserEntity() {

	}

	public UserEntity(int id) {
		this.id = id;
	}

	public UserEntity(UserRequest user) {
		this.firstName= user.getFirstName();
		this.lastName = user.getLastName();
		this.email = user.getEmail();
		this.roleName= user.getRoleName();
		this.userName = user.getUserName();
		this.setStatus(1);
		this.password ="google";
		this.currentScores = 0.0;
		this.previousScores = 0.0;
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Double getPreviousScores() {
		return previousScores;
	}

	public void setPreviousScores(Double previousScores) {
		this.previousScores = previousScores;
	}

	public Double getCurrentScores() {
		return currentScores;
	}

	public void setCurrentScores(Double currentScores) {
		this.currentScores = currentScores;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String role_name) {
		this.roleName = role_name;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

}
