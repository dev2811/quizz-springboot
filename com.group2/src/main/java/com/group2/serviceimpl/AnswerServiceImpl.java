package com.group2.serviceimpl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.group2.dao.AnswerRepository;
import com.group2.entity.AnswerEntity;
import com.group2.service.AnswerService;

@Service
public class AnswerServiceImpl implements AnswerService {

	@Autowired
	private AnswerRepository answerRepository;

	@Override
	public boolean createAnswer(AnswerEntity answer) {
		try {
			answerRepository.save(answer);
		} catch (Exception e) {
			return false;
		}
		return true;
	}
}
