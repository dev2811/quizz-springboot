package com.group2.serviceimpl;

import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.group2.config.LinksUrl;
import com.group2.config.Utils;
import com.group2.converter.UserConverter;
import com.group2.dao.UserRepository;
import com.group2.entity.UserEntity;
import com.group2.request.UserRequest;
import com.group2.response.UserLoginResponse;
import com.group2.response.UserPaginationResponse;
import com.group2.response.UserResponse;
import com.group2.response.UserSearchResponse;
import com.group2.sercurity.JwtService;
import com.group2.sercurity.PasswordEncryption;
import com.group2.service.UserService;
import com.group2.thread.EmailThread;

@Service
public class UserServiceImpl implements UserService {

	Pattern emailPattern = Pattern.compile(
			"\\A(?:[a-z0-9!#$%&'*+=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)\\Z");

	@Autowired
	private JwtService jwtService;

	@Autowired
	private UserRepository userResponsitory;

	@Autowired
	private UserConverter userConverter;

	@Autowired
	private LinksUrl linksUrl;

	@Override
	public UserLoginResponse checkLogin(String username, String password) {
		PasswordEncryption pe = new PasswordEncryption();
		UserLoginResponse userResponse = null;
		String token = "";
		try {
			password = pe.convertHashToString(password);
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		UserEntity userEntity = userResponsitory.findByUserNameAndPassword(username, password);
		if (userEntity != null) {
			userResponse = new UserLoginResponse(userEntity);
			if (userEntity.getStatus() == 1) {
				token = jwtService.generateTokenLogin(userEntity.getUserName());
				userResponse.setToken(token);
				userResponse.setStatus(1);
			} else {
				userResponse.setStatus(0);
			}
		} else {
			userResponse = new UserLoginResponse();
			userResponse.setStatus(2);
		}
		return userResponse;
	}

	@Override
	public UserEntity getUserForAuthen(String username) {
		UserEntity userEntity = userResponsitory.findByUserName(username);
		return userEntity;
	}

	@Override
	public UserEntity findUserByUserName(String username) {
		UserEntity userEntity = userResponsitory.findByUserName(username);
		return userEntity;
	}

	boolean validate(String userName, String password, String email, String lastName, String firstName) {
		try {
			if (userName == null || userName.length() < 8 || userName.length() > 50) {
				return false;
			}
			if (password == null || password.length() < 8 || password.length() > 50) {
				return false;
			}
			if (email == null || !emailPattern.matcher(email).find()) {
				return false;
			}
			if (lastName == null || lastName.length() > 50) {
				return false;
			}
			if (firstName == null || firstName.length() > 50) {
				return false;
			}
			return true;
		} catch (Exception e) {
			return false;
		}

	}

	@Override
	public UserResponse create(UserRequest userRequest) {
		PasswordEncryption pe = new PasswordEncryption();
		UserResponse userResponse = new UserResponse();
		// Lỗi
		userResponse.setStatus(4);
		// Kiểm tra dữ liệu
		if (!validate(userRequest.getUserName(), userRequest.getPassword(), userRequest.getEmail(),
				userRequest.getLastName(), userRequest.getFirstName())) {
			// Sai dữ liệu
			userResponse.setStatus(2);
			return userResponse;
		}
		userRequest.setCurrentScores(0.0);
		userRequest.setPreviousScores(0.0);
		userRequest.setRoleName("ROLE_USER");
		userRequest.setStatus(1);
		try {
			UserEntity user = userConverter.toEntity(userRequest);
			// Kiểm tra username có tồn tại chưa
			if (userResponsitory.findByUserName(userRequest.getUserName()) != null) {
				// Đã tồn tại
				userResponse.setStatus(3);
				return userResponse;
			}
			user.setPassword(pe.convertHashToString(user.getPassword()));
			userResponse = userConverter.toResponse(userResponsitory.save(user));
			sendMail(
					userRequest.getEmail(), linksUrl.getLinksUrlSpringboot() + "api/confirm/email?user-name="
							+ userRequest.getUserName() + "&code=" + Utils.generateMailCode(userRequest.getUserName()),
					userRequest.getUserName());
			// Thành công
			userResponse.setStatus(1);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return userResponse;
	}

	@Override
	public List<UserResponse> getListAllForRank() {
		try {
			List<UserEntity> userEntities = userResponsitory.findByStatus(1, Sort.by("currentScores").descending());
			List<UserResponse> userResponses = new ArrayList<UserResponse>();
			for (UserEntity userEntity : userEntities) {
				userResponses.add(userConverter.toResponse(userEntity));
			}
			return userResponses;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public List<UserResponse> rank(Pageable pageable) {
		try {
			List<UserEntity> userEntities = userResponsitory.findByStatus(1, pageable);
			List<UserResponse> userResponses = new ArrayList<UserResponse>();
			for (UserEntity userEntity : userEntities) {
				userResponses.add(userConverter.toResponse(userEntity));
			}
			return userResponses;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public UserPaginationResponse getAllUsers(int pageNumber) {
		UserPaginationResponse userPaginationResponse = new UserPaginationResponse();
		
		Pageable page = PageRequest.of(pageNumber-1, 11); 
		//Page<UserEntity> userEntities = userResponsitory.findAll(page);
		Page<UserEntity> userEntities = userResponsitory.findByRoleNameEquals("ROLE_USER",page);
		userPaginationResponse.setUsers(userEntities.getContent());
		userPaginationResponse.setTotalPages(userEntities.getTotalPages());
		
		return userPaginationResponse;
	}

	@Override
	public List<UserSearchResponse> getUserByUsername(String username) {
		//List<UserEntity> userEntities = userResponsitory.findByUserNameContainingIgnoreCase(username);
		List<UserEntity> userEntities = userResponsitory.findByUserNameContainingIgnoreCaseAndRoleNameEquals(username, "ROLE_USER");
		List<UserSearchResponse> userResponse = new ArrayList<UserSearchResponse>();
		
		if(userEntities.size() != 0) {
			for (UserEntity userEntity : userEntities) {
				UserSearchResponse user  = new UserSearchResponse();
				user.setId(userEntity.getId());
				user.setUserName(userEntity.getUserName());
				user.setEmail(userEntity.getEmail());
				user.setStatus(userEntity.getStatus());
				
				userResponse.add(user);
			}
		}
		
		return userResponse;
	}

	@Override
	public List<UserSearchResponse> getUserByStatus(Integer status) {
		//List<UserEntity> userEntities = userResponsitory.findByStatus(status);
		List<UserEntity> userEntities = userResponsitory.findByStatusAndRoleNameEquals(status, "ROLE_USER");
		List<UserSearchResponse> userResponse = new ArrayList<UserSearchResponse>();
		
		if(userEntities.size() != 0) {
			for (UserEntity userEntity : userEntities) {
				UserSearchResponse user  = new UserSearchResponse();
				user.setId(userEntity.getId());
				user.setUserName(userEntity.getUserName());
				user.setEmail(userEntity.getEmail());
				user.setStatus(userEntity.getStatus());
				
				userResponse.add(user);
			}
		}
		
		return userResponse;
	}

	@Override
	public UserSearchResponse disableUser(int id) {
		UserSearchResponse userResponse = new UserSearchResponse();
		
		try {
			//UserEntity userEntity = userResponsitory.getOne(id);
			UserEntity userEntity = userResponsitory.findById(id).get();
			userEntity.setStatus(0);
			
			UserEntity updatedUser = userResponsitory.save(userEntity);
			if(updatedUser!=null) {
				userResponse.setId(updatedUser.getId());
				userResponse.setUserName(updatedUser.getUserName());
				userResponse.setEmail(updatedUser.getEmail());
				userResponse.setStatus(updatedUser.getStatus());
			}
		}catch(Exception e){
			e.getStackTrace();
		}finally {
			return userResponse;
		}
	}

	@Override
	public UserSearchResponse activateUser(int id) {
		UserSearchResponse userResponse = new UserSearchResponse();
		try {
			//UserEntity userEntity = userResponsitory.getOne(id);
			UserEntity userEntity = userResponsitory.findById(id).get();
			userEntity.setStatus(1);
			
			UserEntity updatedUser = userResponsitory.save(userEntity);
			if(updatedUser!=null) {
				userResponse.setId(updatedUser.getId());
				userResponse.setUserName(updatedUser.getUserName());
				userResponse.setEmail(updatedUser.getEmail());
				userResponse.setStatus(updatedUser.getStatus());
			}
		}catch(Exception e){
			e.getStackTrace();
		}finally {
			return userResponse;
		}
	}
	@Transactional
	public boolean enableUser(String userName) {
		UserEntity entity = userResponsitory.findByUserName(userName);
		entity.setStatus(1);
		try {
			userResponsitory.save(entity);
		} catch (Exception e) {
			return false;
		}
		return true;
	}

	public void sendMail(String emailReceived, String urlConfirm, String userName) {
		Thread threadMail = new Thread(new EmailThread(emailReceived, urlConfirm, userName));
		threadMail.start();
	}

	@Override
	public UserLoginResponse checkLoginGoogle(UserRequest userRequest) {
		String token = "";
		UserLoginResponse userResponse = new UserLoginResponse();
		UserEntity userEntity = userResponsitory.findByEmail(userRequest.getEmail());
		if (userEntity == null) {
			try {
				userEntity = new UserEntity(userRequest);
				userResponsitory.save(userEntity);
				token = jwtService.generateTokenLogin(userEntity.getUserName());
				userResponse.setToken(token);
				userResponse.setId(userEntity.getId());
				userResponse.setName(userEntity.getUserName());
				userResponse.setRole(userEntity.getRoleName());
				userResponse.setStatus(1);
			} catch (Exception e) {
				userResponse.setStatus(0);
			}
		} else if (userEntity != null && userEntity.getStatus() == 1) {
			userResponse = new UserLoginResponse(userEntity);
			token = jwtService.generateTokenLogin(userEntity.getUserName());
			userResponse.setToken(token);
			userResponse.setStatus(1);
		} else {
			userResponse.setStatus(0);
		}
		return userResponse;
	}
}
