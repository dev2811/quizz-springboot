package com.group2.serviceimpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.group2.dao.UserRepository;
import com.group2.entity.UserEntity;
import com.group2.response.FacebookUserResponse;
import com.group2.sercurity.JwtService;
import com.group2.service.SocialLoginService;

@Service
public class SocialLoginServiceImpl implements SocialLoginService {
	@Autowired
	private JwtService jwtService;

	@Autowired
	private UserRepository userResponsitory;
	
	@Autowired
	private RestTemplate restTemplate;

	@Override
	public FacebookUserResponse getLoginedFacebookUser(String facebookToken) {
		try {
			String url = "https://graph.facebook.com/me?fields= name,first_name,last_name,email&access_token="+facebookToken;
			
			FacebookUserResponse facebookUser = restTemplate.getForObject(url, FacebookUserResponse.class);
			System.out.println(facebookUser);
			
			//save user if not in db
			UserEntity existedUser = userResponsitory.findByUserName(facebookUser.getUserName());
			if(existedUser == null) {
				//UserEntity fbUser = new UserEntity();
				existedUser = new UserEntity();
				existedUser.setFirstName(facebookUser.getFirstName());
				existedUser.setLastName(facebookUser.getLastName());
				existedUser.setEmail(facebookUser.getEmail());
				existedUser.setUserName(facebookUser.getUserName());
				existedUser.setStatus(facebookUser.getStatus());
				existedUser.setRoleName(facebookUser.getRoleName());
				//existedUser.setRoleName("ROLE_ADMIN");
				existedUser.setPassword("");
				existedUser.setCurrentScores(0.0);
				existedUser.setPreviousScores(0.0);
				
				userResponsitory.save(existedUser);
				System.out.println("saved.");
			}
			
			//generate token
			String token = jwtService.generateTokenLogin(existedUser.getUserName());
			facebookUser.setToken(token);
			
			//facebookUser.setRoleName(existedUser.getRoleName());
			return facebookUser;
		}catch(Exception e) {
			return null;
		}
	}

}
