package com.group2.serviceimpl;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.group2.config.Utils;
import com.group2.dao.AnswerRepository;
import com.group2.dao.GradeQuizRepository;
import com.group2.dao.QuestRepository;
import com.group2.dao.QuizRepository;
import com.group2.dao.UserRepository;
import com.group2.entity.AnswerEntity;
import com.group2.entity.GradeEntity;
import com.group2.entity.QuestionEntity;
import com.group2.entity.QuizEntity;
import com.group2.entity.UserEntity;
import com.group2.request.AnswerRequest;
import com.group2.request.QuestRequest;
import com.group2.request.QuestionListRequest;
import com.group2.request.QuestionRequest;
import com.group2.request.UpdateQuestRequest;
import com.group2.response.QuestionResponse;
import com.group2.response.ResultResponse;
import com.group2.response.TotalAndNameResponse;
import com.group2.response.QuestDetailResponse;
import com.group2.response.QuestPaginationResponse;
import com.group2.response.QuestResponse;
import com.group2.service.QuestService;

@Service
public class QuestServiceImpl implements QuestService {

	@Autowired
	private QuestRepository questRepository;

	@Autowired
	private QuizRepository quizRepository;

	@Autowired
	private AnswerRepository answerRepository;

	@Autowired
	private GradeQuizRepository gradeQuizRepository;

	@Autowired
	private UserRepository userResponsitory;

	@Override
	public boolean createQuest(QuestRequest quest) {

		QuizEntity quiz = quizRepository.findByName(quest.getQuizName());
		QuestionEntity questEntity = new QuestionEntity();
		questEntity.setQuiz(quiz);
		questEntity.setContent(quest.getQuestContent());
		if (quest.getAnswer() != null && quest.getListFake() != null) {
			questEntity.setType(2);
		} else if (quest.getListReal() != null && quest.getListFake() != null) {
			questEntity.setType(3);
		} else {
			questEntity.setType(1);
		}
		boolean checkQuest = false;
		boolean checkAnswer = true;
		// add quest
		try {
			questEntity = questRepository.save(questEntity);
			checkQuest = true;
		} catch (Exception e) {
		}
		// add answer 1
		if (checkQuest) {
			if (quest.getAnswer() != null) {
				AnswerEntity answerEntity = new AnswerEntity();
				answerEntity.setCheck(true);
				answerEntity.setQuest(questEntity);
				answerEntity.setContent(quest.getAnswer());
				try {
					answerRepository.save(answerEntity);
				} catch (Exception e) {
					checkAnswer = false;
				}
			}
			// add answer 2
			if (quest.getListFake() != null) {
				for (int i = 0; i < quest.getListFake().size(); i++) {
					AnswerEntity answerEntity = new AnswerEntity();
					answerEntity.setCheck(false);
					answerEntity.setQuest(questEntity);
					answerEntity.setContent(quest.getListFake().get(i));
					try {
						answerRepository.save(answerEntity);
					} catch (Exception e) {
						checkAnswer = false;
					}
				}
			}
			// add answer 3
			if (quest.getListReal() != null) {
				for (int i = 0; i < quest.getListReal().size(); i++) {
					AnswerEntity answerEntity = new AnswerEntity();
					answerEntity.setCheck(true);
					answerEntity.setQuest(questEntity);
					answerEntity.setContent(quest.getListReal().get(i));
					try {
						answerRepository.save(answerEntity);
					} catch (Exception e) {
						checkAnswer = false;
					}
				}
			}
			if (checkAnswer && checkQuest) {
				return true;
			}
			return false;
		} else {
			return false;
		}
	}

	@Override
	public boolean updateQuest(UpdateQuestRequest quest) {
		QuestionEntity questEntity = questRepository.findById(quest.getId());
		questEntity.setContent(quest.getQuestContent());
		if (quest.getAnswer() != null && quest.getListFake() != null) {
			questEntity.setType(2);
		} else if (quest.getListReal() != null && quest.getListFake() != null) {
			questEntity.setType(3);
		} else {
			questEntity.setType(1);
		}
		boolean checkQuest = false;
		boolean checkAnswer = true;
		int checkDekete;
		// update quest
		try {
			questEntity = questRepository.save(questEntity);
			checkQuest = true;
		} catch (Exception e) {
		}
		// delete answer has idUser
		try {
			checkDekete = answerRepository.deleteByQuestIdCustom(questEntity.getId());
		} catch (Exception e) {
			System.out.println(e);
			System.out.println("loi loi loi");
		}
		// add answer 1
		if (checkQuest) {
			if (quest.getAnswer() != null) {
				AnswerEntity answerEntity = new AnswerEntity();
				answerEntity.setCheck(true);
				answerEntity.setQuest(questEntity);
				answerEntity.setContent(quest.getAnswer());
				try {
					answerRepository.save(answerEntity);
				} catch (Exception e) {
					checkAnswer = false;
				}
			}
			// add answer 2
			if (quest.getListFake() != null) {
				List<AnswerEntity> listAnswer = new ArrayList<AnswerEntity>();
				listAnswer = new AnswerEntity().mapListToSave(quest.getListFake(), false, questEntity);
				try {
					answerRepository.saveAll(listAnswer);
				} catch (Exception e) {
					checkAnswer = false;
				}
			}
			// add answer 3
			if (quest.getListReal() != null) {
				List<AnswerEntity> listAnswer = new ArrayList<AnswerEntity>();
				listAnswer = new AnswerEntity().mapListToSave(quest.getListReal(), true, questEntity);
				try {
					answerRepository.saveAll(listAnswer);
				} catch (Exception e) {
					checkAnswer = false;
				}
			}
			if (checkAnswer && checkQuest) {
				return true;
			}
			return false;
		} else {
			return false;
		}
	}

	@Override
	public QuestionResponse getAllQuestFromQuiz(int id) {
		try {
			QuestionResponse questionResponse = new QuestionResponse();
			questionResponse.setQuestions(questRepository.findByQuizId(id));
			if (!questionResponse.getQuestions().isEmpty()) {
				return questionResponse;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public TotalAndNameResponse getTotalQuestAndQuizName(int id) {
		try {
			TotalAndNameResponse reponse = new TotalAndNameResponse();

			QuizEntity quizEntity = quizRepository.findById(id);
			if (quizEntity == null) {
				reponse.setName("");
			} else {
				String name = quizEntity.getName();
				reponse.setName(name.substring(0, 1).toUpperCase() + name.substring(1));
			}

			long totalQuestion = questRepository.countByQuizId(id);
			reponse.setTotal(totalQuestion);
			return reponse;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public ResultResponse getResultForQuiz(QuestionListRequest questions, int quizId, String userName) {
		try {
			// Người dùng chọn
			long attempt = 0;
			// Điểm
			long correct = 0;

			// Kiểm tra user
			UserEntity userEntity = userResponsitory.findByUserName(userName);
			if (userEntity == null) {
				return null;
			}
			// Kiểm tra quiz
			QuizEntity quizEntity = quizRepository.findById(quizId);
			if (quizEntity == null) {
				return null;
			}
			// Kiểm tra question request và data có đúng số câu hỏi
			List<QuestionEntity> countQuestion = questRepository.findByQuizId(quizId);
			if (countQuestion.size() != questions.getQuestions().size()) {
				return null;
			}

			// Vòng lặp câu hỏi request
			for (QuestionRequest item : questions.getQuestions()) {
				// Nếu question id = 0 -> lỗi
				if (item.getId() == 0) {
					return null;
				}
				// Kiểm tra xem câu hỏi đó có tồn tại được so sánh với countQuestion
				QuestionEntity checkQuest = questRepository.getOne(item.getId());
				if (!countQuestion.contains(checkQuest)) {
					return null;
				}

				// Lấy tất cả câu trả lời theo questId
				List<AnswerEntity> answerEntities = answerRepository.findByQuestId(item.getId());
				// Kiểm tra answer của request câu hỏi đó có được chọn không
				long count = item.getAnswers().stream().filter(i -> i.getContent() != "" || i.getId() > 0).count();
				if (count > 0) {
					attempt++;
				}
				if(count == 0) {
					continue;
				}

				// Xét theo kiểu của question type: 1, 2, 3
				switch (item.getType()) {
				case 1:
					// Kiểm tra nội dung content có đúng ko
					if (answerEntities.get(0).getContent().toLowerCase()
							.equals(item.getAnswers().get(0).getContent().toLowerCase())) {
						correct++;
					}
					break;
				case 2:
					// Kiểm tra đc phép chọn 1 sự lựa chọn
					if (answerEntities.get(0).getId() == item.getAnswers().get(0).getId()) {
						correct++;
					}
					break;
				case 3:
					// Sử dụng lại biến để đếm câu trả lời đúng
					count = 0;
					for (AnswerRequest answerRequest : item.getAnswers()) {
						for (AnswerEntity answerEntity : answerEntities) {
							if (answerEntity.getId() == answerRequest.getId()) {
								count++;
							}
						}
					}
					if (count == answerEntities.size()) {
						correct++;
					}
					break;
				default:
					return null;
				}
			}

			// Tính điểm và thêm vào kết quả cho người dùng
			double score = (double) correct / countQuestion.size() * 100;
			GradeEntity gradeEntity = new GradeEntity();
			gradeEntity.setQuiz(quizEntity);
			gradeEntity.setScore(score);
			gradeEntity.setUser(userEntity);
			gradeQuizRepository.save(gradeEntity);

			// Gửi về hiển thị kết quả cho giao diện
			ResultResponse result = new ResultResponse();
			result.setAttempt(attempt);
			result.setCorrect(correct);
			result.setTotalQuestion(countQuestion.size());
			result.setTotalScore(score);
			result.setWrong(countQuestion.size() - correct);

			return result;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public QuestPaginationResponse getListQuestPagination(int  quizId,Pageable pageable) {
		QuestPaginationResponse response = new QuestPaginationResponse();
		Page<QuestionEntity> quests= questRepository.findByQuizId(quizId, pageable);
		List<QuestionEntity> listQuest = quests.stream().collect(Collectors.toList());
		response.setQuests(new QuestResponse().mapTopList(listQuest));
		response.setTotal((int)quests.getTotalElements());
		return response;
	}

	@Override
	public QuestDetailResponse findById(int id) {
		QuestionEntity questEntity = questRepository.findById(id);
		QuestDetailResponse questDetail = new QuestDetailResponse();
		questDetail=questDetail.getQuestBasic(questEntity);
		return questDetail;
	}

	@Override
	public boolean deleteQuest(int id) {
		try {
			questRepository.deleteById(id);
			return true;
		} catch (Exception e) {
			System.out.println(e);
			return false;
		}
	}

	@Override
	public QuestPaginationResponse getListQuestPaginationByName(int quizId, String name) {
		QuestPaginationResponse response = new QuestPaginationResponse();
		List<QuestionEntity> listQuest = questRepository.findByQuizIdAndContentLike(quizId, Utils.getStringSearch(name));
		response.setQuests(new QuestResponse().mapTopList(listQuest));
		return response;
	}

}
