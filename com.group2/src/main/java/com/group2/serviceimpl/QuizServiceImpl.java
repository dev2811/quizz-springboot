package com.group2.serviceimpl;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.group2.dao.QuizRepository;
import com.group2.dao.UserRepository;
import com.group2.entity.QuizEntity;
import com.group2.entity.UserEntity;
import com.group2.request.QuizRequest;
import com.group2.response.QuizPaginationResponse;
import com.group2.response.QuizResponse;
import com.group2.service.QuizService;

@Service
public class QuizServiceImpl implements QuizService {
	@Autowired
	private QuizRepository quizRepository;
	@Autowired
	private UserRepository userRepository;

	/*@Override
	public Page<QuizEntity> getAllQuiz(int pageNumber) {
		Pageable page = PageRequest.of(pageNumber - 1, 11);
		return quizRepository.findAll(page);
	}*/
	
	@Override
	public QuizPaginationResponse getAllQuiz(int pageNumber) {
		QuizPaginationResponse quizPaginationResponse = new QuizPaginationResponse();
		
		Pageable page = PageRequest.of(pageNumber-1, 11); 
		Page<QuizEntity> list = quizRepository.findAll(page);
		quizPaginationResponse.setQuizes(list.getContent());
		quizPaginationResponse.setTotalPages(list.getTotalPages());
		
		return quizPaginationResponse;
	}

	@Override
	public QuizResponse getQuizByName(String name) {
		QuizResponse quizResponse = new QuizResponse();
		quizResponse.setQuizes(quizRepository.findByNameContainingIgnoreCase(name));
		return quizResponse;
	}

	@Override
	public QuizEntity findQuizByName(String quizName) {
		return quizRepository.findByName(quizName);
	}

	@Override
	public boolean addQuiz(QuizRequest wrapper) {
		QuizEntity entity = new QuizEntity();
		UserEntity user = userRepository.findByUserName("hoa1234");
		entity.setStatus(1);
		entity.setName(wrapper.getQuizName());
		entity.setTag(wrapper.getTagName());
		entity.setImage(wrapper.getImage());
		entity.setUser(user);
		try {
			quizRepository.save(entity);
		} catch (Exception e) {
			return false;
		}
		return true;
	}

	@Override
	@Transactional
	public boolean deleteQuiz(int id) {
		try {
			quizRepository.deleteById(id);
			return true;
		}
		catch(EmptyResultDataAccessException e) {
			return false;
		}
	}

	@Override
	public QuizResponse getQuizById(int id) {
		QuizResponse quizResponse = new QuizResponse();
		List<QuizEntity> list = new ArrayList<QuizEntity>();
		try {
			QuizEntity quizEntity=quizRepository.findById(id);
			if(quizEntity == null) {
				return null;
			}
			list.add(quizEntity);
			quizResponse.setQuizes(list);
		}catch (Exception e) {
			e.printStackTrace();
		}
		return quizResponse;
	}

}
