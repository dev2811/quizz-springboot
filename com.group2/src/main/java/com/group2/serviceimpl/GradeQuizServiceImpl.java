package com.group2.serviceimpl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.group2.dao.GradeQuizRepository;
import com.group2.dto.GradeQuizDTO;
import com.group2.entity.GradeEntity;
import com.group2.response.GradeQuizResponse;
import com.group2.service.GradeQuizService;

@Service
public class GradeQuizServiceImpl implements GradeQuizService {
	@Autowired
	private GradeQuizRepository gradeQuizRepository;

	@Override
	public GradeQuizResponse getAllGradeQuizOfUser(String username) {
		GradeQuizResponse response = new GradeQuizResponse();
		//List<GradeQuizDTO> list = new ArrayList<GradeQuizDTO>();
		
		//List<GradeEntity> gradeQuizes =  gradeQuizRepository.findByUserUserNameEquals(username);
		List<GradeQuizDTO> gradeQuizes =  gradeQuizRepository.findGradeQuizOfUser(username);
		
//		gradeQuizes.forEach(gradeQuiz -> {
//			System.out.println(gradeQuiz);
//		});
		
//		gradeQuizes.forEach(gradeQuiz -> {
//			GradeQuizDTO gradeQuizDTO = new GradeQuizDTO(gradeQuiz.getId(), gradeQuiz.getScore(),gradeQuiz.getQuiz());
//			list.add(gradeQuizDTO);
//		});
		
		response.setGradeQuizes(gradeQuizes);
		
		return response;
	}

}
