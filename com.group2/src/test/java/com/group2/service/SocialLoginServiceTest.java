package com.group2.service;

import static org.junit.Assert.*;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.group2.response.FacebookUserResponse;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureTestDatabase(replace = Replace.NONE)
public class SocialLoginServiceTest {
	@Autowired
	private SocialLoginService loginService;
	
	@Test
	public void testGetLoginedFacebookUserWithWrongToken() {
		String wrongFacebookToken = "";
		FacebookUserResponse fbUser = loginService.getLoginedFacebookUser(wrongFacebookToken);
		
		assertEquals(null, fbUser);
	}
	
	@Test
	public void testGetLoginedFacebookUserWithRightToken() {
		String wrongFacebookToken = "EAAFssZAksu24BAO5ZAtXTril1GktDAZBfrNsSrTVhoN7Oq2cw1cFsGlgUa9BLLZCKUFLzKYyirhyZC5eP19wLHjZC5auoGfm9zi9sWOdYkSTHtrxXwIrDhqov6ZBiAJNpQ1RnhmAOiBRjvYZAzq0Kfc74ndmdqexft2tXB35ZAZC4a3SuZAGauJYnBBL3d5bkfp5WU0Wc3GxhAe0RrPtGeZCMOCs";
		FacebookUserResponse fbUser = loginService.getLoginedFacebookUser(wrongFacebookToken);
		
		assertEquals("lan_dfostrm_tran@tfbnw.net", fbUser.getEmail());
	}
}
