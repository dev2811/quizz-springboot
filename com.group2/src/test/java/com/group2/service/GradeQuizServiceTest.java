package com.group2.service;

import static org.junit.Assert.*;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.group2.response.GradeQuizResponse;
import com.group2.service.GradeQuizService;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureTestDatabase(replace = Replace.NONE)
public class GradeQuizServiceTest {
	@Autowired
	private GradeQuizService gradeQuizService;

	@Test
	public void testGetAllGradeQuizOfUser() {
		String username = "phuongtuyet";
		GradeQuizResponse gradeQuizResponse = gradeQuizService.getAllGradeQuizOfUser(username);
		
		assertEquals(2, gradeQuizResponse.getGradeQuizes().size());
	}

}
