package com.group2.service;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.group2.response.UserPaginationResponse;
import com.group2.response.UserSearchResponse;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureTestDatabase(replace = Replace.NONE)
public class UserServiceTest {
	@Autowired
	private UserService userService;
	
	@Test
	@Ignore
	public void testGetAllUser() {
		int numberPage = 1;
		//get users have ROLE_USER
		UserPaginationResponse userResponse = userService.getAllUsers(numberPage);
		
		assertEquals(6, userResponse.getUsers().size());
	}
	
	@Test
	@Ignore
	public void testGetUserByUsername() {
		//get users have ROLE_USER
		List<UserSearchResponse> users = userService.getUserByUsername("hang");
		
		assertEquals(3, users.size());
	}
	
	@Test
	@Ignore
	public void testGetUserByStatusActive() {
		int status = 1;
		//get users have ROLE_USER
		List<UserSearchResponse> users = userService.getUserByStatus(status);
		
		assertEquals(5, users.size());
	}
	
	@Test
	@Ignore
	public void testGetUserByStatusInactive() {
		int status = 0;
		//get users have ROLE_USER
		List<UserSearchResponse> users = userService.getUserByStatus(status);
		
		assertEquals(1, users.size());
	}
	
	@Test
	@Ignore
	public void testDisableUserWithRightId() {
		int id = 3;
		UserSearchResponse user = userService.disableUser(id);
		
		assertEquals("nguyenanh", user.getUserName());
	}
	
	@Test
	@Ignore
	public void testDisableUserWithWrongId() {
		int id = 0;
		UserSearchResponse user = userService.disableUser(id);
		
		assertEquals(null, user.getUserName());
	}
	
	@Test
	@Ignore
	public void testActivateUserWithRightId() {
		int id = 4;
		UserSearchResponse user = userService.activateUser(id);
		
		assertEquals("vuhang12", user.getUserName());
	}
	
	@Test
	@Ignore
	public void testActivateUserWithWrongId() {
		int id = 0;
		UserSearchResponse user = userService.activateUser(id);
		
		assertEquals(null, user.getUserName());
	}
}
