package com.group2.service;

import static org.junit.Assert.*;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.group2.response.QuizPaginationResponse;
import com.group2.response.QuizResponse;
import com.group2.service.QuizService;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureTestDatabase(replace = Replace.NONE)
public class QuizServiceTest {
	@Autowired
	private QuizService quizService;
	
	@Test
	public void testGetAllQuiz() {
		int pageNumber = 1;
		QuizPaginationResponse quizResponse = quizService.getAllQuiz(pageNumber);
		
		assertEquals(4, quizResponse.getQuizes().size());
	}

	@Test
	public void testGetQuizByName() {
		String name = "test";
		QuizResponse quizResponse = quizService.getQuizByName(name);
		
		assertEquals(4, quizResponse.getQuizes().size());
	}

	@Test
	@Ignore
	public void testDeleteQuizWithExistedQuiz() {
		int id = 10;
		boolean check = quizService.deleteQuiz(id);
		
		assertTrue(check);
	}
	
	@Test
	@Ignore
	public void testDeleteQuizWithNotExistedQuiz() {
		int id = 0;
		boolean check = quizService.deleteQuiz(id);
		
		assertFalse(check);
	}
}
